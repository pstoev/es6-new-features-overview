/* jshint esnext: true */

/* Works on Firefox */

(function() {

  'use strict';

  console.log('Demo: arrow syntax');

  // Example 1:
  // 
  var i = 2;
  var sampleArray = [0, 3, 5, 7, 9];

  // Replacing regular functions:
  sampleArray.forEach(digit => {
    console.log(digit);
  });

  // Defining a function:
  var x = (i) => {
    console.log(i * i);
    i++;
  };

  x(i);

  // Example 2:
  // 
  var demoObj = {
    x : 1,
    incrementValue: function() {
      this.x += 1;
      console.log('x is:', this.x);
    },
    delayedIncrement: function() {
      
      var self = this;

      window.setTimeout(function() {
        self.incrementValue();
      }, 1000);

      window.setTimeout(() => this.incrementValue(), 2000);
    }
  };

  demoObj.delayedIncrement();

})();