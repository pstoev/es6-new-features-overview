/* jshint esnext: true */

/* Works on Chrome */

(function() {

  'use strict';

  console.log('Demo: Object.observe');

  var obj = {
    foo: 0,
    bar: 1
  };

  Object.observe(obj, function(changes) {
    console.log(JSON.stringify(changes), '\n');
  });

  setTimeout(function() {
    obj.baz = 2;
  }, 1000);

  setTimeout(function() {
    obj.foo = 'hello';
  }, 2000);

  setTimeout(function() {
    delete obj.baz;
  }, 3000);

})();