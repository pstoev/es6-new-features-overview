/* jshint esnext: true */

/* Works on Firefox/Chrome */

(function() {

  'use strict';

  console.log('Demo: Generators');

  var userData = null;
  var userTweets = null;


  function doFetch(url) {
    console.log('Fetching... ', url);
    return $.ajax({
      url: url,
      dataType: 'json'
    });
  }

  function fetchUser() {
    doFetch('user.json')
      .done(function(userData) {
        goFetch.next(userData);
      });
  }

  function fetchUserTweets() {
    doFetch('tweets.json')
      .done(function(userTweets) {
        goFetch.next(userTweets);
      });
  }

  function *initUser() {
    var user = yield fetchUser();
    var tweets = yield fetchUserTweets();

    var result = {
      user: user,
      tweets: tweets
    };

    console.log(result);

    return result;
  }
  
  // Create generator:
  var goFetch = initUser();

  // Start generator:
  goFetch.next();

})();