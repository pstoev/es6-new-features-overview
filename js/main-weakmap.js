/* jshint esnext: true */

/* Works on Firefox/Chrome */

(function() {

  'use strict';

  console.log('Demo: Weak Maps');

  var holder = function(){}; // Could be any object

  var wm1 = new WeakMap();

  wm1.set(holder, 'value could be anything');
  console.log(wm1.has(holder));
  console.log(wm1.get(holder));

  holder = null; // Throws error in Chrome

  console.log(wm1.get(holder));

  holder = function() {};
  console.log(wm1.get(holder));
})();