/* jshint esnext: true */

/* Works on Chrome/io.js */

(function() {

  'use strict';

  console.log('Demo: let, const syntax');

  // Example 1:
  // 
  let x = 123;
  const Y = 'CANT_CHANGE';

  console.log(x, Y);

  x = 234; // OK
  Y = 'TRY_TO_CHANGE'; // Fail. Throws TypeError
  console.log(x, Y);

  // Example 2:
  // 
  for(var i = 0; i < 10; i++) {
    let j = i * i;
    console.log(j);
  }

  console.log(j); // Fails

})();