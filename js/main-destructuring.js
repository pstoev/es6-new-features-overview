/* jshint esnext: true */

/* Works on Firefox */

(function() {

  'use strict';

  console.log('Demo: Destructuring');

  // Destructoring arrays:
  // 
  var [foo, bar, baz] = [3, 5, 10];

  console.log(foo, bar, baz);

  // Destructuring objects:
  // 
  
  // Option 1.
  var sampleObject = {
    firstName: 'John',
    lastName: 'Doe',
    age: 37
  };

  var {firstName, lastName, age} = sampleObject;
  console.log(firstName, lastName, age);

  // Option 2.
  var {
        firstName: first_name,
        lastName: last_name,
        age: years_old
      } = sampleObject;
  console.log(first_name, last_name, years_old);

})();